const React = require('react');
const { hashHistory } = require('react-router');
import {Button, Icon, Form, Input, Select,Image, Grid, Menu, Table} from 'semantic-ui-react';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
const ReactToastr = require('react-toastr');
const {ToastContainer} = ReactToastr;
const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);
import _ from 'lodash';
import {Scrollbars} from 'react-custom-scrollbars';
class CallCentre extends React.Component {
  constructor()
  {
    super();
    this.state = {
      Patients : [],
      FullData : [],
      activeItem:'Home',
      modal: false,
      item : {},
      FixAppointment : [],
      FixFlag : 0,
      column: null,
      direction: null,
      UniqueCount : 0,
      NewPatientID:'',
      CurrentDate:'',
      AddressFlag: 0,
      FirstName : '',
      LastName: '',
      Age:0,
      ContactNumber:'',
      Gender:'',
      Address: '',
      Email:'',
      DateOfBirth:''
    };
    this.logOut = this.logOut.bind(this);
    this.handleSort = this.handleSort.bind(this);
  }

  componentWillMount(){
    let Patients = [];
    let Trial = [];
    let Count = 0;
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear();
    if(dd<10) {
      dd = '0'+dd
    }
    if(mm<10) {
      mm = '0'+mm
    }
    today = yyyy+ '-' + mm + '-' + dd;
    this.setState({
      CurrentDate: today
    });
    $.ajax({
      url:'/patients/viewPatients',
      type:'GET',
      success: function(data)
      {
        for(let i in data){
          Patients.push({'PatientID':data[i].PatientID,'FirstName':data[i].FirstName,'LastName':data[i].LastName,'Age':data[i].Age,'Gender':data[i].Gender,'ContactNumber':data[i].ContactNumber,DateOfBirth:data[i].DateOfBirth,'Address':data[i].Address,'Appointments':data[i].AppointmentRecord});
          Trial.push(data[i].PatientID);
        }
        this.setState({
          Patients: Patients
        });
        this.setState({
          Fulldata: Patients
        });
        for(let i in Trial){
          Trial[i] = Trial[i].replace('PA','');
        }
        for(let i in Trial){
          Trial[i] = parseInt(Trial[i]);
        }
        let Max = Trial[0];
        for(let i in Trial){
          if(Max < Trial[i]){
            Max = Trial[i];
          }
        }
        Count = Max + 1;
        this.setState({
          UniqueCount: Count
        });
      }.bind(this),
      error: function(err)
      {
        console.log('error occurred on AJAX');
      }.bind(this)
    });
  }

  logOut() {
    cookies.remove('username');
    cookies.remove('role');
    cookies.remove('loginid');
    cookies.remove('emailId');
    hashHistory.push('/');
  }

  handleSort = clickedColumn => () => {
    const { column, Patients, direction } = this.state

    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        Patients: _.sortBy(Patients, [clickedColumn]),
        direction: 'ascending',
      })

      return
    }

    this.setState({
      Patients: Patients.reverse(),
      direction: direction === 'ascending' ? 'descending' : 'ascending',
    })
  }

  render()  {
    const {activeItem} = this.state;
    const { column, direction } = this.state
    let ActiveMenu = '';
    let FormData = '';
    let name = cookies.get('username');
    let role = cookies.get('role');
    let PData = [];
    let Patients = [];
    let context = this;
    let NewAppointment = '';
    let NewPatientID = 'PA' + this.state.UniqueCount;
    if(this.state.Patients != ''){
      for(let i in this.state.Patients ){
        Patients.push(this.state.Patients[i]);
      }
    }
    Patients.map(function(item, key) {
      PData.push({
        "PatientID":item.PatientID,
        "FirstName":item.FirstName,
        "ContactNumber":item.ContactNumber,
        "Address":item.Address
      });
    })
    if(activeItem === 'Home'){
      if(this.state.Patient != ''){
        ActiveMenu = (<div>
          <Table style={{
            marginBottom: '0%'
          }} sortable selectable fixed>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell sorted={column === 'PatientID' ? direction : null} onClick={context.handleSort('PatientID')} >Patient ID</Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'PName' ? direction : null} onClick={context.handleSort('PName')}>Patient Name</Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'PContact' ? direction : null} onClick={context.handleSort('PContact')}>Patient Contact</Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'PAddress' ? direction : null} onClick={context.handleSort('PAddress')} >Patient Address</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
          </Table>
          <Scrollbars renderTrackHorizontal={props => <div {...props} className="track-horizontal" style={{
            display: 'none',
            position: 'right'
          }}/>} autoHeight autoHeightMax={350}>
          <Table striped selectable fixed>
            <Table.Body>
              {Patients.map(function(item, key) {
                return(
                  <Table.Row item={item}><Table.Cell style={{width:"5%"}}>{item.PatientID}</Table.Cell>
                  <Table.Cell style={{width:"5%"}}>{item.FirstName}&nbsp;{item.LastName}</Table.Cell>
                  <Table.Cell style={{width:"5%"}}>{item.ContactNumber}</Table.Cell>
                  <Table.Cell style={{width:"5%"}}>{item.Address}</Table.Cell>
                  {/* <Table.Cell style={{width:"5%"}}><Button  item = {item} color='orange'  onClick={context.displayPatient.bind(context,item)}>view</Button></Table.Cell>*/}
                </Table.Row>);
              })}
            </Table.Body>
          </Table>
        </Scrollbars>
        </div>);
      }
    }

return (
  <div>
    {this.state.modal
      ? <PatientModal data={this.state.item} FullData={this.state.Fulldata} closeModal={this.closeModal.bind(this)}/>
      : null}
      <h1 style={{
        marginTop: '1%',
        marginLeft: '11%',
        color: '#4fcfae'
      }}>Max Health Care
    </h1>
    <span style={{color:'orange'}}>
      <h3 style={{
        marginLeft: '65%',
        marginTop:'-3%',
      }}>Welcome {name}
      (Customer care)</h3>
      <Button onClick={this.logOut.bind(this)} inverted color="orange" style={{
        marginLeft: '80%'
      }}>
      Logout
    </Button>
  </span>
  <Grid divided='vertically'>
    <Grid.Row>
      <Grid.Column width={2}/>
      <Grid.Column width={12}>
        <Menu pointing secondary stackable>
          <Menu.Item name='Home' active={activeItem === 'Home'} onClick={this.handleItemClick} color="orange"/>
        </Menu>
        {FormData}
        <br/>
        {ActiveMenu}
        <div>
          {NewAppointment}</div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
    <ToastContainer ref='regPatient'
      toastMessageFactory={ToastMessageFactory}
      className='toast-top-center' style={{marginTop:'8%'}} />
      <ToastContainer ref='DOB'
        toastMessageFactory={ToastMessageFactory}
        className='toast-top-center' style={{marginTop:'8%'}} />
    </div>
  )}
}
module.exports = CallCentre;
