import ReactDOM from 'react-dom';
import React, {Component} from 'react'
const {hashHistory} = require('react-router');
import Cookies from 'universal-cookie';
const cookies = new Cookies();
import ReactTable from 'react-table';

import {
  Icon,
  Button,
  Card,
  Dimmer,
  Header,
  Form,
  Radio,
  Table
} from 'semantic-ui-react';
const ReactToastr = require('react-toastr');
const {ToastContainer} = ReactToastr;
const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

class childComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      arr: [],
      profile: [],
      date: "",
      time: "",
      item: {},
      aid: 100,
      active1: false,
      count: 0,
      active3: false,
      message: "",
      phid: ""
    }
    this.loadData = this.loadData.bind(this);
  }
  componentWillMount() {
    this.loadData();
  }

  loadData() {
    var pid = cookies.get('username');
    var arr = [];
    var count = 0;
    var temp = [];
    var dateArr = [];
    var i = 0;
    $.ajax({
      url: "/patients/viewAppointment",
      type: 'POST',
      async: false,
      data: {
        pid: pid
      },
      success: function(data) {
        this.setState({message: data[0].Message})
        this.setState({profile: data})
        let length = data[0].PatientID.length;
        let uid = data[0].PatientID.substring(2);
        this.setState({uid:uid});

        if (data[0].AppointmentRecord.length != 0) {
          data[0].AppointmentRecord.map(function(item, key) {
            var num = Number(item.AppointmentID.substring(length));
            temp[i++] = num;
            var b = temp
            count++;
            arr.push({AppointmentID: item.AppointmentID, PreferredDate: item.PreferredDate, Status: item.Status})
          });
          var length1 = temp.length;
          temp.sort(this.sortNumber);
          count = temp[length1 - 1];
          this.setState({
            arr: arr,
            count: count
          }, function() {});
        } else {
          let num1 = 0;
          this.setState({count: num1});
        }

      }.bind(this),
      error: function(err) {
        console.log('error occurred on AJAX');
      }.bind(this)
    });
  }

  logOut() {
    cookies.remove('username');
    cookies.remove('role');
    cookies.remove('loginid');
    cookies.remove('emailId');
    hashHistory.push('/');
  }

  render() {
    let name = cookies.get('username');
let patientHome = '';
    const {active} = this.state
    const {activate} = this.state
    const {activate1} = this.state
    const {activate2} = this.state
    var rowInfo = this.state.arr
    const columns = [
      {
        Header: 'Appointment ID',
        accessor: 'AppointmentID'
      }, {
        Header: 'Date',
        accessor: 'PreferredDate'
      }, {
        Header: 'Status',
        accessor: 'Status'
      }
    ]

        if(cookies.get('username')==undefined) {
        hashHistory.push('/')
        location.reload();
        }
        if(cookies.get('username').length!=0) {
          patientHome=(

            <div>
            <nav className="navbar navbar-default">
              <div className="col-xs-5 col-sm-2 col-md-1">
                <div><img src={this.state.profile[0].ProfilePhoto} className="img-responsive img-circle"  id="imgSize" style={{
            marginLeft: '25%'
          }} /></div>
              </div>
              <div className="col-xs-5 col-sm-8 col-md-9" style={{
                marginTop: '3%'
              }}>
                <p id="spacingTop">{this.state.profile[0].FirstName.lenght != 0
                    ? this.state.profile[0].FirstName + " " + this.state.profile[0].LastName
                    : null}</p>
                <p id="spacingTop">{this.state.profile[0].ContactNumber}</p>
              </div>
              <div className="col-xs-2 col-sm-2 col-md-2" id="logoutPosition">
                <Icon name="log out" style={{
                  float: 'right',
                  cursor: 'pointer'
                }} id='logout' size='large' color='orange' onClick={this.logOut.bind(this)}/>
              </div>
            </nav>
            <h3 style={{
              marginLeft: '3%',
              marginTop: '-1%',
              marginBottom: '1%'
            }}>My Appointment
              </h3>

            {/* React table */}
            <div id="tableSize" style={{}}>
              <ReactTable className="-highlight -striped" data={this.state.arr} columns={columns} defaultPageSize={5} getTdProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: (e, handleOriginal) => {
                    if (handleOriginal) {
                      {
                      }
                    }
                  }
                }
              }}/>
            </div>
            {/* ................... */}
            {/* Alert Card */}
            <div style={{
              margin: '3% 3%'
            }}>
            </div>
            {/* ............. */}
          </div>
        );
      }


        return (
          <div>{patientHome}
        </div>

        );
      }
    }

module.exports = childComponent;
